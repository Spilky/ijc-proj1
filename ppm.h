// ppm.h
// Řešemí IJC-DU1, příklad b), 7.3.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Rozhraní pro mudul ppm.c

//Maximální velikost obrázku
#define IMG_SIZE_MAX 3 * 1000 * 1000

// Struktura pro obrázek
struct ppm {
    unsigned xsize; //Šířka obrázku
    unsigned ysize; //Výška obrázku
    char data[]; // RGB bajty, celkem 3*xsize*ysize
};

// Funkce pro čtení informací o obrázku
struct ppm * ppm_read(const char *filename);

// Funkce pro vytvoření obrázku
int ppm_write(struct ppm *p, const char *filename);
