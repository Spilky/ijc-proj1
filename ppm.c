// ppm.c
// Řešemí IJC-DU1, příklad b), 7.3.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul implementující funkce bitového čtení obrázku a bitového zápisu do obrázku

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm.h"
#include "error.h"

struct ppm * ppm_read(const char * filename)
{
    FILE *fr;
    unsigned long xsize, ysize, color_depth, data_size;
    char typ[3];

    //Pokud se nepodaří otevřít aoubor pro čtení, tak vypiš chybu a vrať NULL
    if((fr = fopen(filename, "rb")) == NULL)
    {
        Error("Soubor se nepodařilo otevřít!\n");
        return NULL;
    }

    //Pokud se nepodaří načíst informace o souboru(hlavičku), tak vypiš chybu, zavři soubor a vrať NULL
    if(fscanf(fr, "%s %lu %lu %lu%*c", typ, &xsize, &ysize, &color_depth) != 4)
    {
        Error("Nepodařilo se načíst informace o obrázku\n");
        if(fclose(fr) == EOF)
        {
            Error("Nepodařilo se zavřít soubor\n");
        }
        return NULL;
    }

    //Pokud typ souboru není obrázek .ppm (P6), tak vypiš chybu, zavři soubor a vrať NULL
    if(strcmp(typ, "P6") != 0)
    {
        Error("Nepodporovaný typ obrázku\n");
        if(fclose(fr) == EOF)
        {
            Error("Nepodařilo se zavřít soubor\n");
        }
        return NULL;
    }

    //Pokud barrevná hloubka obrázku není 255, tak vypiš chybu, zavři soubor a vrať NULL
    if(color_depth != 255)
    {
        Error("Nepodporovaná barevná hloubka\n");
        if(fclose(fr) == EOF)
        {
            Error("Nepodařilo se zavřít soubor\n");
        }
        return NULL;
    }

    struct ppm *img = NULL;         //Ukazatel na strukturu kam se uloží informace o obrázku
    data_size = 3 * xsize * ysize;  //Velikost dat obrázku

    //Pokud je obrázek větší než je podporovaná velikost, tak vypiš chybu, zavři soubor a vrať NULL
    if(data_size > IMG_SIZE_MAX)
    {
        Error("Obrázek je příliš velký!\n");
        if(fclose(fr) == EOF)
        {
            Error("Nepodařilo se zavřít soubor\n");
        }
        return NULL;
    }

    //Pokud se nepodaří uvolnit paměť potřebnou pro uchování dat o obrázku ve struktuře, tak vypiš chybu, zavři soubor a vrať NULL
    if((img = (struct ppm *) malloc(sizeof(struct ppm) + data_size)) == NULL)
    {
        Error("Nepodařilo se naalokovat paměť\n");
        if(fclose(fr) == EOF)
        {
            Error("Nepodařilo se zavřít soubor\n");
        }
        return NULL;
    }

    img->xsize = xsize;     //Šířka obrázku
    img->ysize = ysize;     //Výška obrázku

    //Pokud se nepodaří načíst binární data z obrázku , tak vypiš chybu, uvolni paměť, zavři soubor a vrať NULL
    if(fread(&img->data, 1, data_size, fr) != data_size)
    {
        Error("Nepodařilo se načíst soubor\n");
        free(img);
        if(fclose(fr) == EOF)
        {
            Error("Nepodařilo se zavřít soubor\n");
        }
        return NULL;
    }

    //Pokud se nepodařilo uzavřit soubor, vypiš chybu
    if(fclose(fr) == EOF)
    {
        Error("Nepodařilo se zavřít soubor\n");
    }

    return img;
}

//Funkce pro vytvoření nového souboru formátu .ppm, která zapíše do souboru informace předávané funkci strukturou.
int ppm_write(struct ppm *p, const char * filename)
{
    FILE *fw;
    unsigned long data_size = 3 * p->xsize * p->ysize;  //velikost dat
    unsigned long color_depth = 255;        //barevná hloubka

    //Pokud se nepodaří vytvořit soubor, tak vypiš chybu a vrať -1
    if((fw = fopen(filename, "wb")) == NULL)
    {
        Error("Nepodařilo se vytvořit soubor!\n");
        return -1;
    }

    //Pokude se nepodaří zapsat hlavičku souboru, tak vypiš chybu, zavři soubor a vrať -1
    if(fprintf(fw, "P6 \n%u %u \n%lu\n", p->xsize, p->ysize, color_depth) < 0)
    {
        Error("Nepodařilo se zapsat informace o obrázku!\n");
        if(fclose(fw) == EOF)
        {
            Error("Nepodařilo se zavřít soubor!\n");
        }
        return -1;
    }

    //Pokud se nepodaří zapsat všechny binární data do souboru, tak vypiš chybu, zavři soubor a vrať -1
    if(fwrite(p->data, 1, data_size, fw) != data_size)
    {
        Error("Nepodařilo se vytvořit obrázek!\n");
        if(fclose(fw) == EOF)
        {
            Error("Nepodařilo se zavřít soubor!\n");
        }
        return -1;
    }

    //Pokud se nepodařilo uzavřit soubor, vypiš chybu
    if(fclose(fw) == EOF)
    {
        Error("Nepodařilo se zavřít soubor\n");
    }

    return 0;
}
