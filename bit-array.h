// bit-array.h
// Řešemí IJC-DU1, příklad a), 7.3.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Definice maker pro projekt 1 do předmětu Jazyk C

#include "error.h"

//Počet bitů datového typu unsigned long
#define BITS_UNS_L (sizeof(unsigned long)*8)

//Alokace a nulování bitového pole
//Na 0. indexu pole je ulozena velikost pole v bitech
#define BitArray(jmeno,velikost) unsigned long jmeno[(velikost/BITS_UNS_L) + ((velikost%BITS_UNS_L != 0) ? 2 : 1)] = {(unsigned long)velikost}

typedef unsigned long BitArray_t[];

//Velikost pole v bitech
#define BitArraySize(jmeno) jmeno[0]

//Pokud je definovaný symbol USE_INLINE
#ifdef USE_INLINE

//Inline funkce, která získá hodnotu zadaného bitu, vrací hodnotu 0 nebo 1 (implementována kontrola mezí)
inline unsigned int GetBit(BitArray_t pole, unsigned long index)
{
    if(index < 0 || index >= pole[0])
    {
        FatalError("Chyba: mimo rozsah pole");
        return -1;
    }
    else
    {
        if((pole[1 + index / BITS_UNS_L] & (1UL << index % BITS_UNS_L)) != 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

//Inline funkce, která nastavý zadaný bit v poli na hodnotu danou výrazem (implementována kontrola mezí)
inline void SetBit(BitArray_t pole, unsigned long index, long vyraz)
{
    if(index < 0 || index >= pole[0])
    {
        FatalError("Chyba: mimo rozsah pole");
    }
    else
    {
        if(vyraz == 0)
        {
            pole[1 + index / BITS_UNS_L] &= ~(1UL << index % BITS_UNS_L);
        }
        else
        {
            pole[1 + index / BITS_UNS_L] |= (1UL << index % BITS_UNS_L);
        }
    }
}

//Pokud není definován symbol USE_INLINE
#else

//Získá hodnotu zadaného bitu, vrací hodnotu 0 nebo 1 (bez kontroli mezí)
#define DU1_GET_BIT_(p,i) (((p[1 + i / BITS_UNS_L] & (1UL << i % BITS_UNS_L)) != 0) ? 1 : 0)

//Nastavý zadaný bit v poli na hodnotu danou výrazem
#define DU1_SET_BIT_(p,i,b) (b == 0) ? (p[1 + i / BITS_UNS_L] &= ~(1UL << i % BITS_UNS_L)) : (p[1 + i / BITS_UNS_L] |= (1UL << i % BITS_UNS_L))

//Získá hodnotu zadaného bitu, vrací hodnotu 0 nebo 1 (implementována kontrola mezí)
#define GetBit(jmeno,index) (index < 0 || index >= jmeno[0]) ? FatalError("Chyba: mimo rozsah pole"),0 : DU1_GET_BIT_(jmeno,index)

//Nastavý zadaný bit v poli na hodnotu danou výrazem (implementována kontrola mezí)
#define SetBit(jmeno,index,vyraz) (index < 0 || index >= jmeno[0]) ? FatalError("Chyba: mimo rozsah pole"),0 : DU1_SET_BIT_(jmeno,index,vyraz)

#endif
