// error.c
// Řešemí IJC-DU1, příklad a), 7.3.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Modul pro tisknutí chybových hlášení

#include <stdio.h>
#include "error.h"
#include <stdarg.h>
#include <stdlib.h>

//Funkce pro výpis chyby, vypisuje chybu na standartní chybový výstup.
void Error(const char *fmt, ...)
{
    fprintf(stderr, "CHYBA: ");
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
}

//Funkce pro výpis chyby, vypisuje chybu na standartní chybový výstup a ukončí běh programu
void FatalError(const char *fmt, ...)
{
    fprintf(stderr, "CHYBA: ");
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");

    exit(1);
}
