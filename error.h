// error.h
// Řešemí IJC-DU1, příklad a), 7.3.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Rozhraní pro modul error.c

//Definice funkcí z souboru error.c
void Error(const char *fmt, ...);
void FatalError(const char *fmt, ...);
