// steg-decode.c
// Řešemí IJC-DU1, příklad b), 7.3.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Program, který jako jediný argument přijme obrázek ve formátu PPM a vypíše "tajnou" zprávu v něm uloženou

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include "bit-array.h"
#include "error.h"
#include "ppm.h"

int main(int argc, char *argv[])
{
    struct ppm *img = NULL;

    //Pokud jsou špatně zadáný argumenty, vypiš chybu
    if(argc != 2)
    {
        FatalError("Zadali jste nesprávný počet argumentů\n");
    }
    if((img = ppm_read(argv[1])) == NULL)
    {
        FatalError("Nepodařilo se načíst obrázek\n");
    }

    int i, j;
    //Vytvoření bitového pole
    BitArray(pole,IMG_SIZE_MAX);

    //Eratosthenovo síto
    SetBit(pole,0,1);    //0 není prvočíslo
    SetBit(pole,1,1);    //1 není prvočíslo
    for(i = 2; i <= sqrt(pole[0] - 1); i++)
    {
        if(GetBit(pole,i) == 0)
        {
            for(j = i; j*i <= (pole[0] - 1); j++)
            {
                SetBit(pole,i*j,1);
            }
        }
    }
    //Konec Eratosthenova síta

    int index = 0;
    char znak[2] = {0};    //Pole má 2 prvky, protože makra pocitají s tím, že na 0. indexu je údaj o velikost, na znak[1] se budou ukládat zjištěné bity
    for (i = 2; i < pole[0]; ++i)
    {
        //Pokud je prvočíslo
        if (GetBit(pole,i) == 0)
        {
            //Vymaskuj nejnižší bit a ulož na pozici bitu určující proměnou index
            DU1_SET_BIT_(znak, index, (img->data[i] & 1) );

            //Pokud ještě není načteno 7 prvků, tak zvyš index
            if (index < 7)
            {
                index++;
            }

            //Pokud nebyla načtena koncová nula, tak znak vypiš
            else if (znak[1] != '\0')
            {
                if(isprint(znak[1]))
                {
                    printf("%c",znak[1]);
                }
                index = 0;
                znak[1] = 0;
            }

            //Pokud byla načtena koncová nula, skonči
            else
            {
                break;
            }
        }
    }

    free(img);

    return EXIT_SUCCESS;
}
