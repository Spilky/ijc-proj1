# prvocisla.c
# Řešemí IJC-DU1, příklad a) b), 7.3.2013
# Autor: David Spika (xspilk00), FIT
# Makefile k DU1 do předmětu IJC (Jazyk C)

CFLAGS = -std=c99 -pedantic -Wall -O2

all: prvocisla prvocisla-inline steg-decode


prvocisla.o: prvocisla.c error.h bit-array.h
	gcc $(CFLAGS) -c prvocisla.c -lm -o prvocisla.o

error.o: error.c error.h
	gcc $(CFLAGS) -c error.c -o error.o

ppm.o: ppm.c ppm.h error.h
	gcc $(CFLAGS) -c ppm.c -o ppm.o

steg-decode.o: steg-decode.c error.h bit-array.h ppm.h
	gcc $(CFLAGS) -c steg-decode.c -lm -o steg-decode.o

prvocisla: prvocisla.o error.o
	gcc $(CFLAGS) prvocisla.o -lm error.o -o prvocisla

prvocisla-inline: prvocisla.c error.o
	gcc $(CFLAGS) -DUSE_INLINE prvocisla.c -lm error.o -o prvocisla-inline

steg-decode: steg-decode.o ppm.o error.o
	gcc $(CFLAGS) steg-decode.o -lm ppm.o error.o -o steg-decode



clean:
	rm -f *.o

clean-all: clean
	rm -f prvocisla
	rm -f prvocisla-inline
	rm -f steg-decode
