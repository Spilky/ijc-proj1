// prvocisla.c
// Řešemí IJC-DU1, příklad a), 7.3.2013
// Autor: David Spika (xspilk00), FIT
// Přeloženo: gcc 4.7
// Určení a výpis prvočísel s využitím Eratosthenova síta

#include <stdio.h>
#include <math.h>
#include "bit-array.h"
#include "error.h"

#define BITS 89000000
#define VYPSAT 10

int main(void)
{
    unsigned long vel, i, j;
    //Inicializace bitového pole
    BitArray(pole,BITS);
    //Uložení velikosti pole v bitech do proměnné
    vel = BitArraySize(pole) - 1;

    //Eratosthenovo síto
    SetBit(pole,0,1);    //0 není prvočíslo
    SetBit(pole,1,1);    //1 není prvočíslo
    for(i = 2; i <= sqrt(vel); i++)
    {
        if(GetBit(pole,i) == 0)
        {
            for(j = i; j*i <= vel; j++)
            {
                SetBit(pole,i*j,1);
            }
        }
    }

    unsigned long vypis[10] = {0};

    //Výpis 10 nejvyšších prvočísel
    for(i = vel, j = 0; j < VYPSAT; i--)
    {
        //Pokud je prvočíslo, tak ho vypiš a zvyš počet již zobrazených
        if(GetBit(pole,i) == 0)
        {
            vypis[j] = i;
            j++;
        }
    }

    for(i = 9; i < VYPSAT; i--)
    {
        printf("%lu\n", vypis[i]);
    }

    //Konec
    return 0;
}
